// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "SFTCharStateComponent.generated.h"

DECLARE_MULTICAST_DELEGATE(FOnDead);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class SFT_API USFTCharStateComponent : public UActorComponent
{
	GENERATED_BODY()

public:	

	USFTCharStateComponent();
	FOnDead OnDead;

	void LoseHealthEverySecond();
	void AddHealth();
	void SetHealth(float Value);


	bool IsDead();
protected:

	virtual void BeginPlay() override;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Health")
	float MaxHealth = 100.0f;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Health")
	float Heal = 3.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Timer")
	float StartTimer = 5.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Timer")
	float TimerBetween = 1.0f;

	UFUNCTION()
	void OnTakeAnyDamage(AActor* DamagedActor, float Damage, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser);

	bool bIsHit = false;
	FTimerHandle LoseHealthTimerHandle;

	bool IsPlayerKiller(AActor* DamageActor);

private:
	void HealthDecrease();
	float Health = 0.0f;
};
