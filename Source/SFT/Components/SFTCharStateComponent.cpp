#include "SFTCharStateComponent.h"
#include "Math/UnrealMathUtility.h"
#include "SFTCharacter.h"

USFTCharStateComponent::USFTCharStateComponent()
{
	PrimaryComponentTick.bCanEverTick = false;
	Health = MaxHealth;
}

void USFTCharStateComponent::BeginPlay()
{
	Super::BeginPlay();
	const auto Actor = Cast<AActor>(GetOwner());
	if (!Actor) return;

	Actor->OnTakeAnyDamage.AddDynamic(this, &USFTCharStateComponent::OnTakeAnyDamage);
}

void USFTCharStateComponent::SetHealth(float Value)
{
	if (IsDead()) return;

	Health = FMath::Clamp(Health + Value, 0.0f, MaxHealth);
	UE_LOG(LogTemp, Error, TEXT("Health = %f"), Health);
}

void USFTCharStateComponent::OnTakeAnyDamage(AActor* DamagedActor, float Damage, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser)
{
	bIsHit = true;
	SetHealth(-Damage);
	LoseHealthEverySecond();
	if (Health != 0.0f) return;
	
	OnDead.Broadcast();
	if (!IsPlayerKiller(DamageCauser)) return;
	
}

bool USFTCharStateComponent::IsPlayerKiller(AActor* DamageActor)
{
	const auto Player = Cast<ASFTCharacter>(DamageActor);
	if (!Player || !Player->IsPlayerControlled()) return false;

	Player->AddHealth(Heal);
	return true;
}

void USFTCharStateComponent::LoseHealthEverySecond()
{
	if (!GetWorld()) return;

	if (bIsHit)
	{
		GetWorld()->GetTimerManager().ClearTimer(LoseHealthTimerHandle);
		bIsHit = false;
	}
	GetWorld()->GetTimerManager().SetTimer(LoseHealthTimerHandle, this, &USFTCharStateComponent::HealthDecrease, TimerBetween, true, StartTimer);
}

void USFTCharStateComponent::HealthDecrease()
{
	if (IsDead())
	{
		GetWorld()->GetTimerManager().ClearTimer(LoseHealthTimerHandle);	
	}
	SetHealth(-1.0f);
}

bool USFTCharStateComponent::IsDead()
{
	return Health == 0.0f;
}

