// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SFTGameMode.generated.h"

UCLASS(minimalapi)
class ASFTGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ASFTGameMode();
};



