// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class SFT : ModuleRules
{
	public SFT(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

        PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay", "NavigationSystem", "AIModule" });
        PublicIncludePaths.AddRange(new string[]{"SFT/Components", "SFT/Character","SFT/Weapon","SFT/Items"});
    }
}
