// Fill out your copyright notice in the Description page of Project Settings.


#include "SFTWeaponBase.h"


// Sets default values
ASFTWeaponBase::ASFTWeaponBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ASFTWeaponBase::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ASFTWeaponBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

