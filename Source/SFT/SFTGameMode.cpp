// Copyright Epic Games, Inc. All Rights Reserved.

#include "SFTGameMode.h"
#include "SFTPlayerController.h"
#include "SFTCharacter.h"
#include "UObject/ConstructorHelpers.h"

ASFTGameMode::ASFTGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = ASFTPlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/SFT/Blueprints/Character/BP_BaseCharacter"));
	if (PlayerPawnBPClass.Class != nullptr)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}